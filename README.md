# Yoga

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.19.

## Install Angular CLI

Run `npm install -g @angular/cli`

## Edit proxy

Open `proxy.conf.json` file to edit proxy. Currently, it uses `http://localhost:2900`

## Development server

Run `ng start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build --prod` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
